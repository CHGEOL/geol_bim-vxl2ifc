


## Modul ifc_utils
Funktionen zum schreiben von IFC-Dateien. Mit den Funktionen können einzelne IFC-Entitäten oder ganze
Blöcke geschrieben werden.  
Das Package ist nicht objektorientiert aufgebaut. Die Aufteilung auf drei Dateien dient der Übersichtlichkeit.
- read_write.py: 
  - Templates für den  Header und die Georeferenzierung
  - Öffnen / Speichern von IfcDateien 
  - Hilfsfunktionen
- geometry.py: Funktionen für einzelne Geometrie-Entitäten
- complex_entities: 
  - IfcSite 
  - komplexe Geometrien aus externen Daten (shapely-Objekte)

## Logging
Mit [log_config.py](log_config.py) wird der logger 'geol_bim' konfiguriert. Jede Datei und Klasse erbt den logger und erweitert diesen, z.B. 'ogn.geodata.request.Request' für die Klasse "Request". Der Logger schreibt mit zwei Handlern 
(mit separaten Formattern):
- file: 
  - Level INFO
  - Logfile mit nötigsten Informationen
- console: 
  - detailliert
  - Level DEBUG
  - Auf jeder Zeile ist ersichtlich, aus welchem Modul oder Klasse die Nachricht geschrieben wird. 
