# ====================================================
# PoC to read a voxel file and transform it to ifc
# ====================================================

# Source code base: http://academy.ifcopenshell.org/creating-a-simple-wall-with-property-set-and-quantity-information/
# Utils and other functions also from OGN project

import os
from os import path
import geol_bim.ifc_utils as ifcutl       # Import Methods for writing IFC
import time
import ifcopenshell
import numpy
import pandas as pd 
import yaml
import logging
from logging.config import dictConfig
from geol_bim.log_config import *         # Logging configuration geol_bim

timestamp = time.time()
timestring = time.strftime("%Y%m%dT%H%M%S", time.gmtime(timestamp))

# Load configuration from project
if not os.path.exists('data/output'):
    os.makedirs('data/output')
logging_configuration = log_config(path.join('data', 'output', f'{timestring}.log'))
dictConfig(logging_configuration)  # set config
logger = logging.getLogger('geol_bim')  # start logger
logger.info('Converting Voxel to IFC.')
StartTime = time.time()

try:
    # create variables from the config file
    dir = os.path.dirname(__file__)
    configfile = os.path.join(dir, 'config.yml')
    def get_configfile(_configfile):
        _ReadConfigFile = open(_configfile,'r')
        configuration = yaml.safe_load(_ReadConfigFile)
        _ReadBasicProjectInfo = configuration["BasicProjectInformation"]
        _ColumnNamesForVoxelDefinition = configuration["ColumnsForVoxelDefinition"]
        ColumnNamesFromPSetDefinition=[]
        for key, value in configuration["PropertySetDefinition"].items():
            ColumnNamesFromPSetDefinition.append(value)
        _ColumnNamesPropertySetDefinition = {}
        for i in range(len(configuration["PropertySetDefinition"])):
            _ColumnNamesPropertySetDefinition.update(ColumnNamesFromPSetDefinition[i])
        _for_propertys = configuration["PropertySetDefinition"]
        return(_for_propertys, _ColumnNamesForVoxelDefinition, _ColumnNamesPropertySetDefinition,_ReadBasicProjectInfo) 
    for_propertys, ColumnNamesForVoxelDefinition, ColumnNamesPropertySetDefinition, BasicProjectInfo = get_configfile(configfile)

    # create IFC-header
    template = ifcutl.create_template(BasicProjectInfo["Filename"], BasicProjectInfo["Creator"], BasicProjectInfo["ProjectName"], BasicProjectInfo["Organization"], 
        BasicProjectInfo["SchemaVersion"], minimal = False, file_descr=BasicProjectInfo["ProjectDescription"],  
        project_descr=BasicProjectInfo["ProjectDescription"], #authorization=BasicProjectInfo["Authorization"], 
        origin = list(BasicProjectInfo["ProjectOrigin"].values()), rotation=BasicProjectInfo["Rotation"], 
        georeferencing = "IfcMapConversion" if BasicProjectInfo['LoGeoRef'] == 50 
                            else "IfcGeometricRepresentationContext" if BasicProjectInfo['LoGeoRef'] == 40 
                            else "IfcSite" if BasicProjectInfo['LoGeoRef'] == 30 
                            else print("invalid level of georeferencing") and sys.exit())

    # write template to temp-file
    temp_handle, temp_filename = ifcutl.template2tempfile(template)
    ifcfile, project, owner_history, context = ifcutl.open_ifc(temp_filename)

    # create IfcSite: Site base point (IfcSite.ObjectPlacement) at the project origin (0, 0, 0)
    site, site_placement = ifcutl.create_ifcsite_with_survey_point(ifcfile, project, owner_history, context, origin = list(BasicProjectInfo["ProjectOrigin"].values()),
                                                            rotation=BasicProjectInfo["Rotation"], georef="IfcMapConversion" if BasicProjectInfo['LoGeoRef'] == 30 else "IfcGeometricRepresentationContext" if BasicProjectInfo['LoGeoRef'] == 50 else print("invalid level of georeferencing") and sys.exit(), descr='GEOL_BIM-Site')

    ## Importing .csv File and naming the column names   
    VoxelFile = pd.read_csv(os.path.join(dir,'data', 'input',BasicProjectInfo["VoxelFileName"]), sep ="," )
    ColumnNamesFromConfig = dict(ColumnNamesForVoxelDefinition, **ColumnNamesPropertySetDefinition)
    DictColumnNamesFromConfig = dict((y,x) for x,y in ColumnNamesFromConfig.items())
    VoxelFile.columns = VoxelFile.columns.map(DictColumnNamesFromConfig)

    IfcSpaces = []
    for i, val in VoxelFile.iterrows():
        x = float(val["X_Coordinate"]) - BasicProjectInfo["ProjectOrigin"]["x"]
        y = float(val["Y_Coordinate"]) - BasicProjectInfo["ProjectOrigin"]["y"]
        z = float(val["Z_Coordinate"]) - BasicProjectInfo["ProjectOrigin"]["z"]
        dx = float(val["Extension_X"])
        dy = float(val["Extension_Y"])
        dz = float(val["Extension_Z"])

        space_placement = ifcutl.create_ifclocalplacement(ifcfile, relative_to=site_placement)
        extrusion_placement = ifcutl.create_ifcaxis2placement3D(ifcfile, (x, y, z), (0.0, 0.0, 1.0), (1.0, 0.0, 0.0))                                                  
        #point_list_extrusion_area = [(0.0, 0.0, 0.0), (dx, 0.0, 0.0), (dx, dy, 0.0), (0.0, dy, 0.0), (0.0, 0.0, 0.0)] # Corner Point
        point_list_extrusion_area = [(0.0-(dx/2), 0.0-(dy/2), (-dz/2)), (0.0+(dx/2), 0.0-(dy/2), (-dz/2)), (0.0+(dx/2), 0.0+(dy/2), (-dz/2)), 
                                (0.0-(dx/2), 0.0+(dy/2),(-dz/2)),(0.0-(dx/2), 0.0-(dy/2), (-dz/2))] #Center Point
        extrusion = dz 
        solid = ifcutl.create_ifcextrudedareasolid(ifcfile, point_list_extrusion_area, extrusion_placement, (0.0, 0.0, 1.0), extrusion)
        body_representation = ifcfile.createIfcShapeRepresentation(context, "Body", "SweptSolid", [solid])
        product_shape = ifcfile.createIfcProductDefinitionShape(None, None, [body_representation])
        space = ifcfile.createIfcSpace(ifcutl.create_guid(), owner_history, None, "Voxel", "GEOLOGY_VOXEL", space_placement, product_shape, None, "ELEMENT", "USERDEFINED", None)

        # create property_sets
        PropertyKeyValues = [] # Keys and their values from the config file
        for key, values in for_propertys.items():
            for value in values: 
                PropertyKeyValues.append([key, value])   
        
        for key in for_propertys.keys():
            PropertySingleValues = [] # list with SingleValues, values fromexample 59=IfcPropertySingleValue('volume_factor','volume_factor',IfcText('80.39329512'),$)
            for i in PropertyKeyValues:
                if i[0] == key:
                    PropertySingleValueWriter = ifcfile.createIfcPropertySingleValue("{}".format(i[1]), "{}".format(i[1]), ifcfile.create_entity("IfcReal", float(val["{}".format(i[1])])), None)
                    PropertySingleValues.append(PropertySingleValueWriter)
            property_set = ifcfile.createIfcPropertySet(ifcutl.create_guid(), owner_history, "{}".format(key), None, PropertySingleValues)
            ifcfile.createIfcRelDefinesByProperties(ifcutl.create_guid(), owner_history, None, None, [space], property_set) 
        IfcSpaces.append(space)

    container_space = ifcfile.createIfcRelAggregates(ifcutl.create_guid(), owner_history, "Space Container", None, site, IfcSpaces)
    ifcfile.write(BasicProjectInfo["Filename"])
    logger.info('Successfully done!')

    # time duration to calculate voxel
    def timecalc(_calculatedseconds):
        seconds_in_day = 60 * 60 * 24
        seconds_in_hour = 60 * 60
        seconds_in_minute = 60
        days = _calculatedseconds // seconds_in_day
        hours = (_calculatedseconds - (days * seconds_in_day)) // seconds_in_hour
        minutes = (_calculatedseconds - (days * seconds_in_day) - (hours * seconds_in_hour)) // seconds_in_minute
        return(minutes)
    calculatedseconds = time.time() - StartTime
    logger.info('The script finished in {} minutes'.format(timecalc(calculatedseconds)))
except:
    logger.exception('Got exception on main handler')
    raise