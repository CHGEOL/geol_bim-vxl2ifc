# Stefan Hochuli @ IDIBAU, FHNW
# DATE:    15.07.2019
# NAME:    geometry
# PROJECT: ogn-gefahrenkarte
# ----------------------------------------------------
# Extensions:
# PROJECT:  geol_bim
# DATE:     2021-03-20 / LS 
# ----------------------------------------------------

# Imports
import logging

module_logger = logging.getLogger('geol_bim.ifc_utils.geometry')

# Standard parameters if not specified
O = (0., 0., 0.)  # Location
X = (1., 0., 0.)  # Direction of X-Axis
Y = (0., 1., 0.)  # Direction of Y-Axis, for True North
Z = (0., 0., 1.)  # Direction of Z-Axis


def create_ifcaxis2placement3D(ifcfile, point=O, dirZ=Z, dirX=X):
    """
    Creates an IfcAxis2Placement3D from Location, Axis and RefDirection specified as Python tuples
    :param ifcfile: Instance of ifcopenshell
    :param point: tuple, (x,y,z), Location
    :param dirZ: tuple, Direction of Z-axis
    :param dirX: tuple, Direction of X-Axis
    :return: IfcAxis2Placement3D
    """
    # Convert integer coordinates to float
    point = tuple(float(i) for i in point)
    dirZ = tuple(float(i) for i in dirZ)
    dirX = tuple(float(i) for i in dirX)

    point = ifcfile.createIfcCartesianPoint(point)

    if dirZ == (0., 0., 1.) and dirX == (1., 0., 0.):  # creates less IfcCartesianPoints
        axis2placement = ifcfile.createIfcAxis2Placement3D(point)
    else:
        dirZ = ifcfile.createIfcDirection(dirZ)
        dirX = ifcfile.createIfcDirection(dirX)
        axis2placement = ifcfile.createIfcAxis2Placement3D(point, dirZ, dirX)

    return axis2placement


def create_ifclocalplacement(ifcfile, point=O, dirZ=Z, dirX=X, relative_to=None):
    """
    Creates an IfcLocalPlacement from Location, Axis and RefDirection, and relative placement
    :param ifcfile: Instance of ifcopenshell
    :param point: tuple, (x,y,z), Location
    :param dirZ: tuple, Direction of Z-axis
    :param dirX: tuple, Direction of X-Axis
    :param relative_to: IfcLocalPlacement
    :return: IfcLocalPlacement
    """
    axis2placement = create_ifcaxis2placement3D(ifcfile, point, dirZ, dirX)
    ifclocalplacement = ifcfile.createIfcLocalPlacement(relative_to, axis2placement)

    return ifclocalplacement


def create_pointlist3D(ifcfile, point_list, z=0):
    """
    Creates a list of IfcCartesianPoint
    :param ifcfile: Instance of ifcopenshell
    :param point_list: List with coordinate-tuples, [(x1,y1,z1), (x2,y2,z2), ...]
    :param z: If 2D, add elevation z
    :return: List with IfcCartesianPoint entities [IfcCartesianPoint, IfcCartesianPoint, ...]
    """
    # Quick'n dirty:
    # always create new points
    # Alternative1: filter, if a point already exists
    # Alternative2: IfcCartesianPointList3D -> Creates a List with coordinate tuples, not for each point a new entity

    ifcpts = list()
    for point in point_list:
        # Convert integer coordinates to float
        point = tuple(float(i) for i in point)
        # If 2D, add Z-Value
        if len(point) == 2:
            point = point + (float(z), )
        point = ifcfile.createIfcCartesianPoint(point)
        ifcpts.append(point)
    return ifcpts


def create_ifccartesianpointlist3d(ifcfile, list_points, z=0., precision=3):
    """
    Creates an IfcCartesianPointList3D
    List of 3D-Coordinate-Tuples (No instances of IfcCartesianPoint
    :param ifcfile: Instance of ifcopenshell
    :param list_points: List with coordinate-tuples, [(x1,y1,z1), (x2,y2,z2), ...]
    :param z: If 2D, add elevation z
    :param precision: Precision of point coordinates
    :return: IfcCartesianPointList3D
    """

    if len(list_points[0]) == 2:  # Add elevation
        z = float(z)
        list_points = [(round(float(x), precision), round(float(y), precision), round(z, precision))
                       for x, y in list_points]
    elif len(list_points[0]) == 3:  # Add elevation
        list_points = [(round(float(x), precision), round(float(y), precision), round(float(z), precision))
                       for x, y, z in list_points]
    else:
        module_logger.critical('2D or 3D-points required.')
        raise ValueError('Coordinate tuple not valid.')

    ifcpointlist3d = ifcfile.createIfcCartesianPointList3D(list_points)

    return ifcpointlist3d


def create_ifcpolyloop(ifcfile, list_coordinate_tuples, z=0):
    """
    Creates an IfcPolyLoop (Start =! End)
    :param ifcfile: Instance of ifcopenshell
    :param list_coordinate_tuples: [(x,y,z), (x,y,z)
    :param z: Elevation, if 2D-coordinate tuples
    :return: IfcPolyLoop
    """
    # IfcPolyLoop: First != Last Point (always closed)
    if list_coordinate_tuples[0] == list_coordinate_tuples[-1]:
        list_coordinate_tuples.pop(-1)

    # Create IfcCartesianPoints
    ifcpts_list = create_pointlist3D(ifcfile, list_coordinate_tuples, z)
    # Create IfcPolyloop
    ifcpolyloop = ifcfile.createIfcPolyLoop(ifcpts_list)

    return ifcpolyloop


def create_ifcpolyline(ifcfile, list_coordinate_tuples, z=0, closed=True):
    """
    Creates an IfcPolyLine from a list of points, specified as Python tuples
    :param ifcfile: Instance of ifcopenshell
    :param list_coordinate_tuples: List of Point [(x,y,z)]
    :param z: Elevation, if 2D-coordinate tuples
    :param closed: Closed (True) or open (False) polyline
    :return: IfcPolyline
    """
    # -> IfcIndexedPolyCurve more efficient (IfcCartesianPointList), arcs are possible

    # Closed IfcPolyLine: Fist Point == Last Point
    if closed:
        if list_coordinate_tuples[0] != list_coordinate_tuples[-1]:
            list_coordinate_tuples.append(list_coordinate_tuples[0])
    # Create IfcCartesianPoints
    ifcpts_list = create_pointlist3D(ifcfile, list_coordinate_tuples, z)
    # Create IfcPolyline
    ifcpolyline = ifcfile.createIfcPolyLine(ifcpts_list)

    return ifcpolyline


def create_ifcextrudedareasolid(ifcfile, point_list, ifcaxis2placement, extrude_dir=(0., 0., 1.), depth=1):
    """
    Creates an IfcExtrudedAreaSolid from a list of points, specified as Python tuples
    :param ifcfile: Instance of ifcopenshell
    :param point_list: List of Point [(x,y,z)]
    :param ifcaxis2placement: IfcAxis2Placement
    :param extrude_dir: tuple, direction for extrusion
    :param depth: float, depth of the extrusion
    :return:
    """
    # not used yet
    ifcpolyline = create_ifcpolyline(ifcfile, point_list)
    ifcclosedprofile = ifcfile.createIfcArbitraryClosedProfileDef('AREA', None, ifcpolyline)
    ifcdirection = ifcfile.createIfcDirection(extrude_dir)
    ifcextrudedareasolid = ifcfile.createIfcExtrudedAreaSolid(ifcclosedprofile, ifcaxis2placement, ifcdirection, depth)

    return ifcextrudedareasolid
