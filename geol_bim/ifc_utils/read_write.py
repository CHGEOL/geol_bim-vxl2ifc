# Stefan Hochuli @ IDIBAU, FHNW
# DATE:    15.07.2019
# NAME:    read_write
# PROJECT: ogn-gefahrenkarte
# ----------------------------------------------------
# Extensions:
# PROJECT:  geol_bim
# DATE:     2021-03-20 / LS 
# ----------------------------------------------------


# Imports
import uuid
import time
import tempfile
import os
from math import radians, sin, cos
from urllib.parse import unquote, urlencode  # Send/Receive swisstopo-REST
from urllib.request import urlopen
import json
import logging

import ifcopenshell

module_logger = logging.getLogger('geol_bim.ifc_utils.read_write')

# Standard parameters if not specified
O = (0., 0., 0.)  # Location
X = (1., 0., 0.)  # Direction of X-Axis
Y = (0., 1., 0.)  # Direction of Y-Axis, for True North
Z = (0., 0., 1.)  # Direction of Z-Axis

# Creates GUID
create_guid = lambda: ifcopenshell.guid.compress(uuid.uuid1().hex)


def create_template(filename, author, project_name, organization, schema_version, minimal,
                    file_descr='', project_descr=None, authorization='', origin=O, rotation=0.,
                    georeferencing='IfcGeometricRepresentationContext'):
    """
    Creates the template for an ifc_utils-file
    :param filename: str
    :param author: str
    :param project_name: str
    :param organization: str
    :param schema_version: 'IFC2X3' or 'IFC4'
    :param minimal: Bool
                        if True, write a minimal Header without OwnerHistory, GeometricRepresentationContext, Units
    :param file_descr: str
                        Optional File Description for STEP-Header
    :param project_descr: str
                        Optional Project Description for IfcProject
    :param authorization: str
                        Optional Auth-orization for STEP-Header
    :param origin: Coordinate-Tuple of the project reference point / survey point in the global reference system.
    :param rotation: Rotation FROM true North TO positive y-Axis
                     Degrees, counterclockwise (right-handed Cartesian coordinate system)
                     Rotation center == origin
                     Rotation of the underlying project coordinate system,
                     relative to the true north (geographic northing direction).

    :param georeferencing:  '': No Georef
                            'IfcSite': Georef with IfcSite (Not within Template)
                            'IfcGeometricRepresentationContext': Georef with IfcGeometricRepresentationContext
                            'IfcMapConversion': Georef with IfcGeometricRepresentationContext AND IfcMapConversion



    :return: template
                        String with STEP-header and first section of an IFC-File
    """

    app, app_version = 'IfcOpenShell', ifcopenshell.version
    project_globalid = create_guid()
    t = time.time()
    t_string = time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime(t))  # yyyy-mm-ddThh:mm:ss
    x_direction = rotation2xdir(rotation)
    true_north = rotation2ydir(rotation)

    module_logger.debug('Check schema versions...')
 ## ifcopenshell.schema_identifier does not exist anymore.... --> what is the alternative method/attribute?   
    #module_logger.debug(f'\tChosen IFC-schema:\t\t{schema_version}')
    #module_logger.debug(f'\tIntalled IFC-schema:\t{ifcopenshell.schema_identifier}')
    #if ifcopenshell.schema_identifier not in schema_version:
    #    module_logger.critical('\tSchemas do not match!\n')

    # STEP Header https://en.wikipedia.org/wiki/ISO_10303-21
    header = f"""ISO-10303-21;
HEADER;
FILE_DESCRIPTION(
/* description */ ('{file_descr}'),
/* implementation_level */ '2;1');
FILE_NAME(
/* name */ '{filename}',
/* time_stamp */ '{t_string}',
/* author */ ('{author}'),
/* organization */ ('{organization}'),
/* preprocessor_version */ '{app}',
/* originating_system */ '{app}',
/* authorization */ '{authorization}');
FILE_SCHEMA(('{schema_version}'));
ENDSEC;
"""
#FILE_SCHEMA(('{ifcopenshell.schema_identifier}'));  --> this line shoud be in 100


    # ifc_utils header
    if minimal:  # Only IfcProject TODO -> IfcProjectLibrary? For Classification / Pset / Types
        data = f"""
DATA;
#1 = IFCPROJECT('{project_globalid}',$,'{project_name}','{project_descr}',$,$,$,$,$);
ENDSEC;
END-ISO-10303-21;
"""

    else:  # Full Template with Representation Context, Units, etc.
        data = f"""
DATA;
#1=IFCPERSON($,$,'{author}',$,$,$,$,$);
#2=IFCORGANIZATION($,'{organization}',$,$,$);
#3=IFCPERSONANDORGANIZATION(#1,#2,$);
#4=IFCAPPLICATION(#2,'{app_version}','{app}','');
#5=IFCOWNERHISTORY(#3,#4,$,.ADDED.,{int(t)},#3,#4,{int(t)});
#6=IFCDIMENSIONALEXPONENTS(0,0,0,0,0,0,0);
#7=IFCSIUNIT(*,.LENGTHUNIT.,$,.METRE.);
#8=IFCSIUNIT(*,.AREAUNIT.,$,.SQUARE_METRE.);
#9=IFCSIUNIT(*,.VOLUMEUNIT.,$,.CUBIC_METRE.);
#10=IFCSIUNIT(*,.PLANEANGLEUNIT.,$,.RADIAN.);
#11=IFCMEASUREWITHUNIT(IFCPLANEANGLEMEASURE(0.017453292519943295),#10);
#12=IFCCONVERSIONBASEDUNIT(#6,.PLANEANGLEUNIT.,'DEGREE',#11);
#13=IFCUNITASSIGNMENT((#7,#8,#9,#12));
"""
        if georeferencing == 'IfcGeometricRepresentationContext':
            # Georeferencing (Translation, Rotation with the placement of the GeometricRepresentationContext
            # If no georef needed, x_direction=X, origin=O, true_north=Z[0:2]
            module_logger.debug('Georeferencing with IfcGeometricRepresentationContext')
            georef = f"""
#14=IFCDIRECTION({x_direction});
#15=IFCDIRECTION((0.,0.,1.));
#16=IFCCARTESIANPOINT({origin});
#17=IFCAXIS2PLACEMENT3D(#16,#15,#14);
#18=IFCDIRECTION({true_north[0:2]});
#19=IFCGEOMETRICREPRESENTATIONCONTEXT($,'Model',3,1.E-05,#17,#18);
#20=IFCPROJECT('{project_globalid}',#5,'{project_name}','{project_descr}',$,$,$,(#19),#13);
ENDSEC;
END-ISO-10303-21;
"""
        elif georeferencing == 'IfcMapConversion':
            module_logger.debug('Georeferencing with IfcMapConversion')
            georef = f"""
#14=IFCDIRECTION({X});
#15=IFCDIRECTION((0.,0.,1.));
#16=IFCCARTESIANPOINT({O});
#17=IFCAXIS2PLACEMENT3D(#16,#15,#14);
#18=IFCDIRECTION({true_north[0:2]});
#19=IFCGEOMETRICREPRESENTATIONCONTEXT($,'Model',3,1.E-05,#17,#18);
#20=IFCPROJECT('{project_globalid}',#5,'{project_name}','{project_descr}',$,$,$,(#19),#13);
#21=IFCPROJECTEDCRS('EPSG:2056','CH1903+ / LV95 -- Swiss CH1903+ / LV95','CH1903+','LN02','CH1903+ / LV95',$,#7);
#22=IFCMAPCONVERSION(#19,#21,{origin[0]},{origin[1]},{origin[2]}, {x_direction[0]},{x_direction[1]},$);
ENDSEC;
END-ISO-10303-21;
"""
        else:
            module_logger.debug('No georeferencing in IfcGeometricRepresentationContext or IfcMapConversion')
            georef = f"""
#14=IFCDIRECTION({X});
#15=IFCDIRECTION((0.,0.,1.));
#16=IFCCARTESIANPOINT({O});
#17=IFCAXIS2PLACEMENT3D(#16,#15,#14);
#18=IFCDIRECTION({Y[0:2]});
#19=IFCGEOMETRICREPRESENTATIONCONTEXT($,'Model',3,1.E-05,#17,#18);
#20=IFCPROJECT('{project_globalid}',#5,'{project_name}','{project_descr}',$,$,$,(#19),#13);
ENDSEC;
END-ISO-10303-21;
"""

        data = data + georef

    template = header + data

    return template


def template2tempfile(template):
    """
    Writes the template to a tempfile.
    :param template: str, IFC header and first lines of the DATA section
    :return:
        temp_handle:    handle to the open file
        temp_filename:  filename
    """
    temp_handle, temp_filename = tempfile.mkstemp(suffix='.ifc')
    with open(temp_filename, 'w') as file:
        file.write(template)
    module_logger.debug('Writing tempfile...')

    return temp_handle, temp_filename


def open_ifc(filename, minimal=False):
    """
    Open file with ifcopenshell, get references
    :param filename: str, for example temp_filename
    :param minimal: Bool
                    if True, open an IFC without OwnerHistory, GeometricRepresentationContext, Units
    :return: entities of ofcopenshell
        ifcfile:        representing the file
        project:        representing IfcProject (minimal=False)
        owner_history:  representing IfcOwnerHistory (minimal=False)
        context:        representing IfcContext (minimal=False)
    """
    # Open file with ifcopenshell, get references
    ifcfile = ifcopenshell.open(filename)
    module_logger.debug('Loading tempfile, obtain references...')
    if minimal:
        project = ifcfile.by_type("IfcProject")[0]
        return ifcfile, project
    else:
        # Obtain references to instances defined in template
        owner_history = ifcfile.by_type("IfcOwnerHistory")[0]
        project = ifcfile.by_type("IfcProject")[0]
        context = ifcfile.by_type("IfcGeometricRepresentationContext")[0]

        return ifcfile, project, owner_history, context


def save_ifc(ifcfile, path, temp_handle=None, temp_filename=None):
    """
    Saves the Ifc-File, closes the temporary file
    :param ifcfile: entity of IfcOpenShell
    :param path: str, path to save the file
    :param temp_handle: optional
    :param temp_filename: optional
    :return:
    """
    # Write
    ifcfile.write(path)

    # Delete Temporary File
    if temp_handle:
        os.close(temp_handle)
    if temp_filename:
        os.remove(temp_filename)

    module_logger.info(f'\nFile written to {path}\n')


def rotation2xdir(rot, mode='deg'):
    """
    Creates a 3D coordinate-tuple of the X-direction
    :param rot: Rotation (counterclockwise)
    :param mode: 'deg' or 'rad'
    :return: Coordinate-tuple of x-direction, (1,0,0) for rot=0
    """
    if mode == 'deg':
        rotation = radians(rot)
    elif mode == 'rad':
        rotation = rot
    else:
        print("Rotation: in degrees ('deg') or radians ('rad')")
        raise ValueError

    # Cosmetics: if no rotation -> 1,0,0
    if rot == 0:
        xdirection = (1., 0., 0.)
    else:
        xdirection = (round(cos(rotation), 5), round(sin(rotation), 5), 0.)

    return xdirection


def rotation2ydir(rot, mode='deg'):
    """
    Creates a 3D Coordinate-Tuple of the Y-Direction
    Only used to calculate True North (for Axis2Placement, X-/Z-Directions are used)
    :param rot: Rotation (counterclockwise)
    :param mode: 'deg' or 'rad'

    :return: Coordinate-tuple of Y-direction (1,0,0) for rot=0
    """
    if mode == 'deg':
        rotation = radians(rot)
    elif mode == 'rad':
        rotation = rot
    else:
        print("Rotation: in degrees ('deg') or radians ('rad')")
        raise ValueError

    # Cosmetics: if no rotation -> 0,1,0
    if rot == 0:
        tn = (0., 1., 0.)
    else:
        tn = (round(sin(rotation), 5), round(cos(rotation), 5), 0.)

    return tn


def decdeg2dms(dd):
    """
    Conversion DecimalDegrees to DegreeMinuteSecondMillionthsecond
    For georeferencing IfcSite (RefLatitude, RefLongitude
    :param dd: Coordinate in DecimalDegree, e.g. 8.966213906595765
    :return: degrees, minutes, seconds, millionthseconds, e.g. (8, 57, 58, 370064)
    Adapted from:
    https://stackoverflow.com/questions/2579535/convert-dd-decimal-degrees-to-dms-degrees-minutes-seconds-in-python
    """
    # If degrees are negative (not the case in Switzerland)
    is_positive = dd >= 0
    dd = abs(dd)

    minutes, seconds = divmod(dd*3600, 60)
    seconds, millionthseconds = divmod(seconds*1000000, 1000000)
    degrees, minutes = divmod(minutes, 60)
    degrees = degrees if is_positive else -degrees
    if is_positive:
        return int(degrees), int(minutes), int(seconds), int(round(millionthseconds))
    else:
        return -int(degrees), -int(minutes), -int(seconds), -int(round(millionthseconds))


def lv95towgs84(easting, northing):
    """
    Convert LV95 to WGS84 (Degrees, Minutes, Seconds, Millionthseconds)
    Send Request to Reframe-REST-API
    For georeferencing IfcSite (RefLatitude, RefLongitude
    :param easting: in LV95 (2714805)
    :param northing: in LV95 (1276941)
    :return: lon, lat; e.g. (8, 57, 58, 370064), (47, 37, 59, 71019)
    """
    params = {'easting': easting,
              'northing': northing,
              'format': 'json'}
    uri = 'http://geodesy.geo.admin.ch/reframe/lv95towgs84?' + unquote(urlencode(params))

    wgs84 = urlopen(uri)
    data = wgs84.read()
    wgs_84_dd = json.loads(data)
    lon_dms = decdeg2dms(float(wgs_84_dd['easting']))
    lat_dms = decdeg2dms(float(wgs_84_dd['northing']))

    return lon_dms, lat_dms


def replace_umlaut(string):
    """
    Replaces umlauts
    :param string: string, potentially with umlaut
    :return: str, without umlaut
    """
    table = {ord('Ä'): 'ae',
             ord('ä'): 'ae',
             ord('Ü'): 'ue',
             ord('ü'): 'ue',
             ord('Ö'): 'oe',
             ord('ö'): 'oe'}
    return string.translate(table, )
