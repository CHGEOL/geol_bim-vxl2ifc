# Stefan Hochuli @ IDIBAU, FHNW
# DATE:    15.07.2019
# NAME:    complex_entities
# PROJECT: ogn-gefahrenkarte
# ----------------------------------------------------
# Extensions:
# PROJECT:  geol_bim
# DATE:     2021-03-20 / LS 
# ----------------------------------------------------

# Imports
import logging


from .geometry import *
from .read_write import create_guid, lv95towgs84, rotation2xdir

module_logger = logging.getLogger('geol_bim.ifc_utils.complex_entities')



# some weird namings...?  -->not survey_point but Site_base_point (IfcSite.ObjectPlacement) ?
# some reengineering for use in geol_bim is preferable
# .....
def create_ifcsite_with_survey_point(ifcfile, project, owner_history, context, origin, rotation=0, georef='',
                                     descr='Grundstueck'):
    """
    IfcSite,
    ShapeRepresentation as the Product Base Point (0/0/0)
    :param ifcfile: Instance of ifcopenshell
    :param owner_history: IfcOwnerHistory
    :param project: IfcProject
    :param context: IfcGeometricRepresentationContext
    :param origin: tuple, coordinate of project origin for georeferencing
    :param rotation: project rotation to world coordinate system for georeferencing
    :param georef: georeferencing method
                if georef == 'IfcSite
                -> Georeferencing with the placement of IfcSite
    :return: IfcSite, IfcLocalPlacement
    """
    try:
        lon, lat = lv95towgs84(origin[0], origin[1])
    except URLError:
        lon = None
        lat = None
        module_logger.warning('No Internet Connection. No Lon/Lat in IfcSite')

    elevation = origin[2]

    # Survey Point at the Project Origin
    survey_point = ifcfile.createIfcCartesianPoint((0., 0., 0.))
    # GeometricCurveSet
    geom_curve_set = ifcfile.createIfcGeometricCurveSet([survey_point])
    site_representation = ifcfile.createIfcShapeRepresentation(context, 'ProductBasePoint', 'GeometricCurveSet',
                                                               [geom_curve_set])
    site_product_shape = ifcfile.createIfcProductDefinitionShape('Georeferenzierung', None, [site_representation])
    
    # Site placement, all other entities should be relative to the site placement
    if georef == 'IfcSite':  # Do georeferencing with IfcSite
        module_logger.debug('Georeferencing with Placement of IfcSite')
        site_placement = create_ifclocalplacement(ifcfile, origin, dirZ=(0., 0., 1.), dirX=rotation2xdir(rotation))
    else:  # No Georef or Done with IfcMapConversion OR IfcGeometricRepresentationContext
        site_placement = create_ifclocalplacement(ifcfile)
    
    # create IfcSite
    site = ifcfile.createIfcSite(create_guid(), owner_history, descr, 'ProjectBasePoint = SurveyPoint',
                                 None, site_placement, site_product_shape, None, 'ELEMENT', lat, lon, elevation, None,
                                 None)
    container_project = ifcfile.createIfcRelAggregates(create_guid(), owner_history, "Project Container",
                                                       None, project, [site])
    return site, site_placement



def create_ifc_triangulatedfaceset(ifcfile, triangles, points):
    """
    Creates a simple triangulated facs from a triangulation, no holes
    :param ifcfile:
    :param triangles: Triangulated Surface, List of Triangles, referencing to to vertices in points
    :param points: List of 3D-Points
    :return:
    """
    ifccartesianpointlist3d = create_ifccartesianpointlist3d(ifcfile, points, precision=3)
    ifctriangulatedfaceset = ifcfile.createIfcTriangulatedFaceSet(ifccartesianpointlist3d, None, False, triangles)

    return ifctriangulatedfaceset



