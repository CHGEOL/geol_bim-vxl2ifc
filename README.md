# GEOL_BIM-Vxl2ifc

Transformation von Voxelgeometrien in das IFC-Format.
 <br> 
  <br> 
 
# 1. Übersicht
Mit diesem Python-Skript werden Voxeldaten nach IFC transformiert. Dabei gelten folgende Rahmenbedingungen und Transformationsregeln:
* Quellformat: Die Voxeldaten sind im Format .csv bereitzustellen. 
  * Die Voxel werden definiert über ihre jeweilige Zentrumskoordinate sowie die individuelle Ausdehnung in die drei Richtungen x, y, z.
  * Die Koordinatenwerte der Voxel müssen im Koordinatensystem LV95 vorliegen.
  * Für jedes Voxel können beliebige Eigenschaften definiert werden (als frei definierbare Spalten der csv-Datei). Die Transformation dieser Eigenschaften ins Datenmodell von IFC kann über die Konfiguration gesteuert werden.
  * --> Das Modell zur Darstellung der Voxel und die Struktur der csv-Datei werden weiter unten im Detail beschrieben (Abschnitt Konfiguration).
* Zielformat: Die Voxel werden nach IFC transformiert.
  * IFC-Version: 4x1
  * Aus jedem Voxel wird ein einzelner IfcSpace erzeugt. Der Objekttyp des IfcSpaces wird als 'GEOLOGY_VOXEL' gesetzt (IfcSpace.ObjectType).
  * Die Voxel werden in der Raumstruktur direkt der IfcSite angefügt.
  * Die Art der Georeferenzierung in IFC kann gewählt werden. Es wird dazu das Konzept der "Level of Georeferencing" (LoGeoRef) verwendet und die Level 30, 40, 50 unterstützt.


## 1.1. Installation: 
 1. Download Anaconda
 2. Download Gitlab Repository
 3. Erstellen der Entwicklungssumgebung aus der yml-Datei im Installationsordner: `conda env create -f py37geol_bim.yml`
 4. Download IfcOpenShell für Python 3.7 (http://ifcopenshell.org/python), lokal ablegen: _C:\Anaconda3\envs\py37geol_bim\Lib\site-packages_
 5. Aktivieren der neuen Umgebung in der Anaconda Prompt mit dem Befehl: `conda activate py37geol_bim`
 

## 1.2. Benutzung:

 - Exportieren der Voxelstruktur aus der 3D-Software als .csv-Datei und diese ablegen in den Ordner "data/input"
 - Anpassung des config.yml **NUR** an den kommentierten respektive den weiter unten im Readme beschriebenen Stellen anpassen
 - Verwendung der Datei "vxl2ifc.py", für die Erstellung der IFC-Datei mit Voxel-Darstellung 
 - Zum Test kann das .csv File 'VoxelCreationTestFile.csv' (im Ordner data\input)verwendet werden, es enthält nur sehr wenige Zeilen und ist in kurzer Zeit (< 1 Minute) ausgeführt

 <br>  

## 1.3. Konfiguration - Verwendung .config FIle:
Der Transferprozess kann über eine Konfiguration gesteuert werden. Die Konfigurationsdatei kann an verschiedenen Stellen für die eigene Verwendung angepasst werden. Diese Variablen sind im Abschnitt `Beschreibung der Variablen aus dem config.yml` beschrieben.

Zur Berechnung der Voxeldarstellung wird ein Input .csv File aus dem Export der 3D Software (zum Beispiel Leapfrog) benötigt.

Mit Hilfe der Variablen im Abschnitt "ColumnsForVoxelDefinition" des config.yml Files werden die Spaltennamen aus dem exportierten .csv File auf die Struktur des vxl2csv.py Skripts angepasst. Das genaue Vorgehen wird in den folgenden zwei Abschnitten erklärt:
  
### 1.3.1. **ColumnsForVoxelDefinition**: 
Die Variablen in diesem Abschnitt sind unabdingbar. Sie dürfen nicht ergänzt werden und es darf keine dieser Variablen weggelassen werden, da sie essenziell für die Konstruktion der einzelnen Voxel sind. Die Variablennamen auf der linken Seite dürfen **NICHT** verändert werden. Auf der rechten Seite stehen die Spaltennamen aus dem vom Benutzer erstellten .csv File.
 <br>  
  -> Zuordnung der Spalte im .csv File welche die X-Koordinaten erhält:
 <br> 

*Beipielversion*: X_Coordinate: Center:0   
*Angepasste Version*: x_coordinate: benutzerabhängiger Spaltenname mit X-Koordinaten  <br>   
 <br> 

### 1.3.2. **PropertySetDefinition:** 
Der interne Variablenname wird hier einem Property und einem Property Set zugewiesen. Die Definitionen der Property Sets und der Propertys können in der Konfigurationsdatei frei definiert werden. Die Variablen müssen jedoch im gleichen Format, wie im Beispiel zu sehen, angegeben werden
 <br>  
 -> Definition einer eigenen Variable und Zurodnung zum PropertySet:
 <br> 
*Beispielversion*: MaxPermeability: LogKmax  
*Angepasste Version*: benutzerdefinierter Variablenname: benutzerdefinierter Spaltenname im .csv File 

<br> 

# 2. Beschreibung der Variablen aus dem config.yml file:
| Variable | Beschreibung |
| ----------- | ----------- |
|  |  |
| **BasicProjectInformation** | **Benutzerdefinierte Werte werden den vorgegbenen Variablen zugeordnet**
|VoxelFileName| Name der Input .csv Datei
|Creator| Initialen / Name des Benutzers
|Filname| Dateiname des erstellten .ifc Files
|ProjectDescription| Projektbeschreibung für IfcProject
|ProjectName| Projektname
|IfcSiteName|Name des Grundstücks 
|ProjectOrigin | Projektnullpunkt, Angabe der x-, y-, z-Koordinaten in LV95 Koordinaten| 
|Rotation| Projektrotation zum Weltkoordinatensystem für die Georeferenzierung (Drehung VON echtem Norden zur positiven y-Achse in Grad, gegen den Uhrzeigersinn (rechtshändiges kartesisches Koordinatensystem). Rotationszentrum == Ursprung. Drehung des zugrundeliegenden Projektkoordinatensystems, relativ zum wahren Norden (geografische Nordrichtung))
|SchemaVersion|IFC Version (IFC 2.3 oder IFC 4.1)
|LoGeoRef| Level of Georeferencing (= Georeferenzierungsebene). Angabe der Level 30, 40 oder 50. Weitere Informationen unter: https://jgcc.geoprevi.ro/docs/2019/10/jgcc_2019_no10_3.pdf |
| ----------- | ----------- |
| **ColumnsForVoxelDefinition** |  **Benutzerdefinierte Werte werden den vorgegbenen Variablen zugeordnet**
| {}_Coordinate | Spalte im .csv File mit den jeweiligen LV95- X, Y, Z- Zentrumskoordinaten des einzelnen Voxel|
| Extension_{} | Ausdehnung des Voxel in die jewilige Koordinatenrichtung|
| ----------- | ----------- |
| **PropertySetDefinition** | **Benutzerdefinierte Werte werden den benutzderdefinierten Variablen zugeordnet**
| GEOL_BIM_GeologicFeatureCommon | Name des PropertySets|
| Uncertainty| Unsicherheitswert
